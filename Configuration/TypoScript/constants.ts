
plugin.tx_hiveexthistory_hivehistorylisthistory {
    view {
        # cat=plugin.tx_hiveexthistory_hivehistorylisthistory/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_history/Resources/Private/Templates/
        # cat=plugin.tx_hiveexthistory_hivehistorylisthistory/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_history/Resources/Private/Partials/
        # cat=plugin.tx_hiveexthistory_hivehistorylisthistory/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_history/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveexthistory_hivehistorylisthistory//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveexthistory_hivehistoryshowhistory {
    view {
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_history/Resources/Private/Templates/
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_history/Resources/Private/Partials/
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_history/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveexthistory {

        persistence {
            storagePid =
        }

        model {
            HIVE\HiveExtHistory\Domain\Model\History {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

plugin.tx_hiveexthistory_hivehistoryshowhistory.persistence.storagePid = 123

plugin.tx_hiveexthistory_hivehistoryrendersliderhistory{
    view {
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_history/Resources/Private/Templates/
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_history/Resources/Private/Partials/
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_history/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveexthistory_hivehistoryshowhistory//a; type=string; label=Default storage PID
        storagePid =
    }
}