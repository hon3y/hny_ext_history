<?php
namespace HIVE\HiveExtHistory\Controller;

/***
 *
 * This file is part of the "hive_ext_history" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * HistoryController
 */
class HistoryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * historyRepository
     *
     * @var \HIVE\HiveExtHistory\Domain\Repository\HistoryRepository
     * @inject
     */
    protected $historyRepository = null;


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        // get settings
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }

        // get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];

        // get selected histories if not empty
        $oHistories = $this->historyRepository->findByUidListOrderByListIfNotEmpty($aSettings['oHiveExtHistory']['main']['mn'], $aSettings['oHiveExtHistory']['main']['orderElements']);

        // get all histories if no selection
        if($oHistories === NULL) {
            $oHistories = $this->historyRepository->findAll($iLimit = 999, $iOffset = 0, $aSettings['oHiveExtHistory']['main']['orderElements']);
        }

        // slice histories if limit is set
        $iMaxItems = (int)$aSettings['oHiveExtHistory']['main']['maxItems'];
        if ($iMaxItems != NULL) {
            $oHistories = array_slice($oHistories, 0, $iMaxItems);
        }

        $this->view->assign('oHistories', $oHistories);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtHistory\Domain\Model\History $history
     * @return void
     */
    public function showAction(\HIVE\HiveExtHistory\Domain\Model\History $history)
    {
        // get metadata
        $oMetaData = $history->getMetaData();
        if ($oMetaData != '') {
            $sMetaObjectAlternativeTitle = $oMetaData->getAlternativeTitle();
            $sMetaObjectKeywords = $oMetaData->getKeywords();
            $sMetaObjectDescription = $oMetaData->getDescription();
        }
        // metaTITLE
        if ($sMetaObjectAlternativeTitle != '') {
            $sMetaTitleContent = strip_tags($sMetaObjectAlternativeTitle);
        } else {
            $sMetaTitleContent = strip_tags($history->getTitle());
        }
        $sOgMetaTitle = '<meta name="og:title" content="' . $sMetaTitleContent . '">';
        $sDCMetaTitle = '<meta name="DC.title" content="' . $sMetaTitleContent . '">';
        $GLOBALS['TSFE']->page['title'] = $sMetaTitleContent;
        $this->response->addAdditionalHeaderData($sOgMetaTitle . PHP_EOL . $sDCMetaTitle);
        // metaKEYWORDS
        if ($sMetaObjectKeywords != '') {
            $sMetaKeywordsContent = strip_tags($sMetaObjectKeywords);
            $sMetaKeywords = '<meta name="keywords" content="' . $sMetaKeywordsContent . '">';
            $sOgMetaKeywords = '<meta name="og:keywords" content="' . $sMetaKeywordsContent . '">';
            $sDCMetaKeywords = '<meta name="DC.keywords" content="' . $sMetaKeywordsContent . '">';
            $this->response->addAdditionalHeaderData($sMetaKeywords . PHP_EOL . $sOgMetaKeywords . PHP_EOL . $sDCMetaKeywords);
        }
        // metaDESCRIPTION
        if ($sMetaObjectDescription != '') {
            $sMetaDescriptionContent = strip_tags($sMetaObjectDescription);
//        } else {
//            $sMetaDescriptionContent = strip_tags($history->getLongDescription());
//        }
            $sMetaDescription = '<meta name="description" content="' . $sMetaDescriptionContent . '">';
            $sOgMetaDescription = '<meta name="og:description" content="' . $sMetaDescriptionContent . '">';
            $sDCMetaDescription = '<meta name="DC.description" content="' . $sMetaDescriptionContent . '">';
            $this->response->addAdditionalHeaderData($sMetaDescription . PHP_EOL . $sOgMetaDescription . PHP_EOL . $sDCMetaDescription);
        }

        $this->view->assign('history', $history);
    }

    /**
     * action renderSlider
     *
     * @return void
     */
    public function renderSliderAction()
    {
        // get settings
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }

        // get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];

        // get selected histories if not empty
        $oHistories = $this->historyRepository->findByUidListOrderByListIfNotEmpty($aSettings['oHiveExtHistory']['main']['mn'], $aSettings['oHiveExtHistory']['main']['orderElements']);

        // get all historys if no selection
        if(count($oHistories) == 0) {
            $oHistories = $this->historyRepository->findAll($iLimit = 999, $iOffset = 0, $aSettings['oHiveExtHistory']['main']['orderElements']);
        }

        // slice histories if limit is set
        $iMaxItems = (int)$aSettings['oHiveExtHistory']['main']['maxItems'];
        if ($iMaxItems > 0) {
            $oHistories = array_slice($oHistories, 0, $iMaxItems);
        }

        //render slides to array
        $aSlides = array();
        $aNavLabelsOverrides = [];
        foreach ($oHistories as $oSlide) {
            $configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            //$standaloneView = $this->objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);

            $standaloneView = $this->objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');

            $extensionName = $this->request->getControllerExtensionName();

            $standaloneView->getRequest()->setControllerExtensionName($extensionName);
            $standaloneView->setFormat('html');


            //$standaloneView->setLayoutRootPaths($configuration['view']['layoutRootPaths']);
            $standaloneView->setPartialRootPaths($configuration['view']['partialRootPaths']);
            $standaloneView->setTemplateRootPaths($configuration['view']['templateRootPaths']);

            $standaloneView->setTemplate('History/RenderHistory');



            //$aSlide = json_decode(json_encode($oSlide), true);



            $standaloneView->assign('aSettings', $aSettings);
            $standaloneView->assign('history', $oSlide);
            $result = $standaloneView->render();

            $aSlides[] = $result;


        }

        $aNavLabelsOverrides = [];
        foreach ($oHistories as $oSlide) {
            $aNavLabelsOverrides[] = $oSlide->getDate()->format('Y');
        }

        $this->view->assign('aSlides', $aSlides);
        $this->view->assign('aNavLabelsOverrides', $aNavLabelsOverrides);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);


    }
}



