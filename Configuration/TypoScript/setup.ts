
plugin.tx_hiveexthistory_hivehistorylisthistory {
    view {
        templateRootPaths.0 = EXT:hive_ext_history/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistorylisthistory.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_history/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistorylisthistory.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_history/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistorylisthistory.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveexthistory_hivehistorylisthistory.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hiveexthistory_hivehistoryshowhistory {
    view {
        templateRootPaths.0 = EXT:hive_ext_history/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryshowhistory.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_history/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryshowhistory.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_history/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryshowhistory.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveexthistory_hivehistoryshowhistory.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveexthistory._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-history table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-history table th {
        font-weight:bold;
    }

    .tx-hive-ext-history table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)




## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveexthistory{

        # Standard PID aus den Konstanten
        persistence {
            storagePid = {$plugin.tx_hiveexthistory.persistence.storagePid}
        }

        model {
            HIVE\HiveExtHistory\Domain\Model\History {
                persistence {
                    storagePid = {$plugin.tx_hiveexthistory.model.HIVE\HiveExtHistory\Domain\Model\History.persistence.storagePid}
                }
            }
        }

        classes {
            HIVE\HiveExtHistory\Domain\Model\History.newRecordStoragePid = {$plugin.tx_hiveextcontact.model.HIVE\HiveExtCountry\Domain\Model\History.persistence.storagePid}
            HIVE\HiveExtHistory\Domain\Model\History {
                mapping {
                    recordType = 0
                    tableName = sys_category
                }
            }
        }

        settings {
            production {
                list {
                    itemsPerPage = 4
                    maximumNumberOfLinks = 3
                }
            }
        }

    }
}

plugin.tx_hiveexthistory_hivehistoryrendersliderhistory {
    view {
        templateRootPaths.0 = EXT:hive_ext_history/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryrendersliderhistory.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_history/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryrendersliderhistory.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_history/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveexthistory_hivehistoryrendersliderhistory.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveexthistory_hivehistoryrendersliderhistory.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}