<?php
namespace HIVE\HiveExtHistory\UserFunc;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class FieldSelectorUserFunc
{
    public function getFieldSelection(&$params)
    {
        // create option list
        $optionList = array();

        // get storagePid
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $fullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $sStoragePid = $fullTyposcriptSettings['plugin.']['tx_hiveexthistory_hivehistorylisthistory.']['persistence.']['storagePid'];

        // get data
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_hiveexthistory_domain_model_history');
        $queryBuilder
            ->select('*')
            ->from('tx_hiveexthistory_domain_model_history')
            ->where('sys_language_uid IN (-1,0)');

        // only use if soragePid set
        if($sStoragePid != '') {
            $queryBuilder->andWhere('pid IN ( :string )');
            $queryBuilder->setParameter(':string', explode(',',$sStoragePid), \Doctrine\DBAL\Connection::PARAM_INT_ARRAY);
        }

        $data = $queryBuilder->execute()->fetchAll();

        foreach($data as $item){
            $date = date('Y-m-d', $item['date']);
            $label = $date . ' [' . $item['uid'] .'] ' . $item['title'];
            $value = $item['uid'];
            $optionList[] = array(0 => $label, 1 => $value);
        }

        // return config
        $params['items'] = array_merge($params['items'], $optionList);
        return $params;
    }
}