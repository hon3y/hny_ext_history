<?php
namespace HIVE\HiveExtHistory\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_history" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * History
 */
class History extends \HIVE\HiveExtArticle\Domain\Model\Article
{
    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * detailtext
     *
     * @var string
     */
    protected $detailtext = '';

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the detailtext
     *
     * @return string $detailtext
     */
    public function getDetailtext()
    {
        return $this->detailtext;
    }

    /**
     * Sets the detailtext
     *
     * @param string $detailtext
     * @return void
     */
    public function setDetailtext($detailtext)
    {
        $this->detailtext = $detailtext;
    }
}
