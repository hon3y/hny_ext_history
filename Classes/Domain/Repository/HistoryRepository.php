<?php
namespace HIVE\HiveExtHistory\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_history" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Historys
 */
class HistoryRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtHistory\\Domain\\Model\\History';
        $sUserFuncPlugin = 'tx_hiveexthistory';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string $uidList
     * @param string $orderElements
     * @return \HIVE\HiveExtHistory\Domain\Model\History
     */
    public function findByUidListOrderByListIfNotEmpty($uidList, $orderElements = "ASC")
    {
        $result = [];

        if ($uidList != "") {
            $uidArray = explode(',', $uidList);


            if ($orderElements == "DESC") {
                for ($i = (count($uidArray) - 1); $i >= 0; $i-- ) {
                    $result[] = $this->findByUid($uidArray[$i]);
                }
            } else {
                foreach ($uidArray as $uid) {
                    $result[] = $this->findByUid($uid);
                }
            }
        }

        return $result;

    }


    /**
     * Find by Uid
     *
     * @param int $uid
     * @return History
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

    /**
     * findAll
     *
     * @param int $iLimit
     * @param int $iOffset
     * @param string $orderElements
     * @return
     */
    public function findAll($iLimit = 999, $iOffset = 0, $orderElements = "ASC")
    {
        $query = $this->createQuery();
        //$query = $query->matching($query->logicalAnd($query->equals('publish', 1)));

        if ($orderElements == "DESC") {
            $query->setOrderings(array(
                'date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
            ));
        } else {
            $query->setOrderings(array(
                'date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
            ));
        }

        $query->setOffset((int) $iOffset);
        $query->setLimit((int) $iLimit);
        return $query->execute();
    }
}