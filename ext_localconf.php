<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtHistory',
            'Hivehistorylisthistory',
            [
                'History' => 'list'
            ],
            // non-cacheable actions
            [
                'History' => 'list'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtHistory',
            'Hivehistoryshowhistory',
            [
                'History' => 'list, show'
            ],
            // non-cacheable actions
            [
                'History' => 'list, show'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtHistory',
            'Hivehistoryrendersliderhistory',
            [
                'History' => 'renderSlider, show'
            ],
            // non-cacheable actions
            [
                'History' => 'renderSlider, show'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivehistorylisthistory {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_history') . 'Resources/Public/Icons/user_plugin_hivehistorylisthistory.svg
                        title = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistorylisthistory
                        description = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistorylisthistory.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveexthistory_hivehistorylisthistory
                        }
                    }
                    hivehistoryshowhistory {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_history') . 'Resources/Public/Icons/user_plugin_hivehistoryshowhistory.svg
                        title = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryshowhistory
                        description = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryshowhistory.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveexthistory_hivehistoryshowhistory
                        }
                    }
                    hivehistoryrendersliderhistory {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_history') . 'Resources/Public/Icons/user_plugin_hivehistoryrendersliderhistory.svg
                        title = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryrendersliderhistory
                        description = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryrendersliderhistory.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveexthistory_hivehistoryrendersliderhistory
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey, $globals)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivehistorylisthistory >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivehistorylisthistory {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = History (list)
                            description = List of histories
                            tt_content_defValues {
                                CType = list
                                list_type = hiveexthistory_hivehistorylisthistory
                            }
                        }
                        show := addToList(hivehistorylisthistory)
                    }

                }
            }'
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivehistoryshowhistory >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivehistoryshowhistory {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = History (show)
                            description = Show content of selected History
                            tt_content_defValues {
                                CType = list
                                list_type = hiveexthistory_hivehistoryshowhistory
                            }
                        }
                        show := addToList(hivehistoryshowhistory)
                    }

                }
            }'
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivehistoryrendersliderhistory >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivehistoryrendersliderhistory {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = History (slider)
                            description = Slider of histories
                            tt_content_defValues {
                                CType = list
                                list_type = hiveexthistory_hivehistoryrendersliderhistory
                            }
                        }
                        show := addToList(hivehistoryrendersliderhistory)
                    }

                }
            }'
        );

    }, $_EXTKEY, $GLOBALS
);
